# configurable, easy to change:
from settings import credentials, constants

BASEBALL_ASSIGNMENT_TOPIC = 'baseball.page.assignment'
BASEBALL_PLAYER_DATA_TOPIC = 'baseball.data.player'
BASEBALL_PLAYER_ERROR_TOPIC = 'baseball.data.error'

DATABASE_CONNECTION = 'postgresql://{user}:{password}@{url}:{port}/domaindb'.format(
    user=credentials.DATABASE_USER, password=credentials.DATABASE_PASSWORD, url=constants.postgres_url,
    port=constants.postgres_port)

DATABASE_ECHO = False

CONSUMER_REPLAY = True
