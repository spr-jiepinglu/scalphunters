import datetime
import json
import uuid

import requests

from decorators.headers import cover_up
from bs4 import BeautifulSoup

from persistence.models.raw_stats import PlayerProfile, RawPlayerStats
from utils import common_utils

DATA_SOURCE = 'fangraph'


@cover_up
def get_fangraph(url='', params='', headers=None, **kwargs):
    k_headers = kwargs['coverup-headers']
    if headers is not None:
        k_headers.update(headers)
    return requests.get(url=url, params=params, headers=k_headers)


def player_profile_from_stats(page):
    soup = BeautifulSoup(page)
    name = soup.select('h1')[0].text
    infos = soup.find_all('div', {'class': 'player-info-box-item'})
    bats_throws = list(filter(lambda info: 'Bats' in info.text and 'Throws' in info.text, infos))[0].text
    bats = bats_throws.split(':')[1].strip().split('/')[0]
    throws = bats
    if len(bats_throws.split(':')[1].strip()) > 2:
        throws = bats_throws.split(':')[1].strip()[2]
    pos = list(soup.find_all('div', {'class': 'player-info-box-pos'})[0].text.split('/'))

    name_alias = [name]
    return PlayerProfile(id=uuid.uuid4(), name=name, name_alias=json.dumps(name_alias), position=json.dumps(pos),
                         bats=bats, throws=throws)


def map_row_to_stats(row):
    player_stats = RawPlayerStats(datasource=DATA_SOURCE, created_date=datetime.datetime.now(),
                                  last_modified_date=datetime.datetime.now())
    condition = {}
    time_period = {}
    if 'Team' in row:
        player_stats.team = row.pop('Team')
        condition['team'] = player_stats.team
    if 'Season' in row:
        time_period['season'] = row.pop('Season')
    player_stats.conditions = json.dumps(condition)
    player_stats.time_period = json.dumps(time_period)
    player_stats.stats = json.dumps(row)
    
    return player_stats


# return dict
def parse_row(row_tag, heads):
    tds = list(map(lambda td: td.text, row_tag.find_all('td')))
    return dict(zip(heads, tds))


# should return list of player stats raw (no-relationship)
# table tag is souped
def parse_table(table_tag):
    thead = table_tag.find('thead')
    tbody = table_tag.find('tbody')
    row_tags = list(map(lambda tr: tr.extract(), tbody.find_all('tr')))
    heads = list(map(lambda th: th.text, thead.find_all('th')))

    rows = list(map(lambda row_tag: parse_row(row_tag=row_tag, heads=heads), row_tags))
    stats = list(map(lambda row: map_row_to_stats(row), rows))
    return stats


def parse_tables(stats_page):
    soup = BeautifulSoup(stats_page)
    tables = soup.find_all('table', {'class': 'rgMasterTable'})
    return common_utils.flatmap(map(lambda t: parse_table(t), tables))
