import datetime
from multiprocessing import Process

from data.agents import Commander, Scout
from messengers import kafka_messenger
from persistence.models.raw_stats import RequestRecord, RequestError
from persistence.utils import domain_utils
from scenarios import fangraph_utils
from settings import configs
from utils import crawler_utils, archivist

logger = archivist.logger('fangraph')

FANGRAPH_PREFIX = 'https://www.fangraphs.com/'
FANGRAPH_PLAYERS = FANGRAPH_PREFIX + 'players.aspx'
FANGRAPH_PLAYER_STATS = FANGRAPH_PREFIX + 'statss.aspx?playerid={}'
KAFKA_KEY = 'fangraph'
GROUPS = 'python_fangraph_scout'

exporter = kafka_messenger.exporter()
letter_box = kafka_messenger.letter_box(GROUPS)
request_manager = domain_utils.RequestRecordManager()
profile_manager = domain_utils.PlayerProfileManager()
error_manager = domain_utils.RequestErrorManager()

LISTENING_TOPICS = [configs.BASEBALL_ASSIGNMENT_TOPIC]


class FangraphCommander(Commander):

    def __init__(self, kafka_producer, request_manager):
        Commander.__init__(self)
        self.producer = kafka_producer
        self.request_manager = request_manager

    def assign_request(self, request):
        # 1. log db 2. send messages
        existed = self.request_manager.first_by_request(request)
        if existed is None:
            record = RequestRecord(request=request, fetched_date=datetime.datetime.now())
            self.producer.send(topic=configs.BASEBALL_ASSIGNMENT_TOPIC, key=KAFKA_KEY, value=record.to_map())
            self.request_manager.save_record(record)
        elif existed.handled_date is None and existed.handled_date is None:
            # not handled yet, no need to re assign
            existed.fetched_date = datetime.datetime.now()
            self.request_manager.save_record(existed)
        else:
            # handled, re-do
            existed.handled_date = None
            existed.handler = None
            existed.parsed_date = None
            existed.fetched_date = datetime.datetime.now()
            self.producer.send(topic=configs.BASEBALL_ASSIGNMENT_TOPIC, key=KAFKA_KEY, value=existed.to_map())
            self.request_manager.save_record(existed)

    def player_from_letter_directory(self, letter_directory_url):
        logger.info("{} parsing letter directory: {}".format(self.name, letter_directory_url))
        directory = fangraph_utils.get_fangraph(url=letter_directory_url).content
        player_links = crawler_utils.all_links(directory)
        cleansed = list(
            filter(lambda link: link is not None and 'stats' in link and crawler_utils.has_param(link, 'playerid'),
                   player_links))
        player_requests = list(map(lambda filtered: FANGRAPH_PREFIX + filtered, cleansed))
        for req in player_requests:
            self.assign_request(req)

    # repeat for day?
    def iterate_players(self):
        # todo handle request error response
        player_directory = fangraph_utils.get_fangraph(url=FANGRAPH_PLAYERS).content

        all_links = crawler_utils.all_links(player_directory)
        filtered_links = list(
            filter(lambda link: link is not None and crawler_utils.has_param(link, 'letter'), all_links))
        letters_link = list(map(lambda filtered: FANGRAPH_PREFIX + filtered, filtered_links))

        for link in letters_link:
            self.player_from_letter_directory(letter_directory_url=link)


class FangraphScout(Scout):

    def __init__(self, kafka_producer, kafka_consumer, request_manager, player_profile_manager, error_manager):
        Scout.__init__(self)
        self.producer = kafka_producer
        self.consumer = kafka_consumer
        self.request_manager = request_manager
        self.player_profile_manager = player_profile_manager
        self.error_manager = error_manager
        self.consumer.subscribe(LISTENING_TOPICS)

    def handle_player_message(self, message):
        logger.info(' {} WORKING: Handling message {}'.format(self.name, message))
        if 'request' in message:
            request_url = message['request']
            stats_page = fangraph_utils.get_fangraph(url=request_url).content

            # handle user
            profile = fangraph_utils.player_profile_from_stats(stats_page)
            saved_profile = self.player_profile_manager.save_profile(profile)

            # handle table metrics, todo: more efficient
            metrics = fangraph_utils.parse_tables(stats_page)
            for metric in metrics:
                metric.name_id = saved_profile.id
                metric.player_profile = saved_profile
                self.producer.send(topic=configs.BASEBALL_PLAYER_DATA_TOPIC, key=KAFKA_KEY, value=metric.to_map())

            request_record = self.request_manager.first_by_request(request=request_url)
            request_record.handled_date = datetime.datetime.now()
            self.request_manager.save_record(request_record)

            logger.info('{} Handled {}'.format(self.name, profile.name))

        else:
            logger.error("request not in message {}".format(message))

    def handle_player_messages(self):
        for message in self.consumer:
            try:
                self.handle_player_message(message.value)
            except Exception as e:
                logger.error('wrong message {}, exception {}'.format(message, e))
                error = RequestError(request=message.value['request'], error_message=str(e),
                                     handled_date=datetime.datetime.now())
                self.producer.send(topic=configs.BASEBALL_PLAYER_ERROR_TOPIC, key=KAFKA_KEY, value=error.to_map())
                self.error_manager.save_error(error)


def spawn_one_scout():
    scout = FangraphScout(kafka_producer=exporter, kafka_consumer=letter_box, request_manager=request_manager,
                          player_profile_manager=profile_manager, error_manager=error_manager)
    scout_process = Process(target=scout.handle_player_messages())
    scout_process.start()


def spawn_one_commander():
    commander = FangraphCommander(kafka_producer=exporter, request_manager=request_manager)
    commander_process = Process(target=commander.iterate_players())
    commander_process.start()


def start_deliver_once():
    # for i in range(0, 4):
    spawn_one_scout()
    spawn_one_commander()
