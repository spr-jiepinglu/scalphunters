class OrmUtils:
    def __init__(self, session):
        self.session = session

    def insert(self, instance):
        self.session.add(instance)
        self.session.commit()

    def create(self, orm_cls, **kwargs):
        self.insert(orm_cls(**kwargs))

    def delete(self, instance):
        self.session.delete(instance)
        self.session.commit()
