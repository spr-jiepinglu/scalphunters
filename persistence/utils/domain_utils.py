import json
from contextlib import contextmanager

from sqlalchemy import create_engine, inspect
from sqlalchemy.orm import sessionmaker

from persistence.models.raw_stats import RequestRecord, RawPlayerStats, PlayerProfile, RequestError
from persistence.utils import orm_utils
from settings import configs
from utils import archivist

engine = create_engine(configs.DATABASE_CONNECTION, echo=configs.DATABASE_ECHO)

logger = archivist.logger('domain util')


@contextmanager
def session_scope():
    session_maker = sessionmaker(bind=engine)
    session = session_maker()
    try:
        yield session
        session.commit()
    except Exception as e:
        logger.error("sql session error {}".format(e))
        session.rollback()
        raise
    finally:
        session.close()


with session_scope() as session:
    util = orm_utils.OrmUtils(session)


def to_dict(sql_obj):
    return {c.key: getattr(sql_obj, c.key) for c in inspect(sql_obj).mapper.column_attrs}


def sql_copy(from_sql_obj, to_sql_obj):
    for column in inspect(from_sql_obj).mapper.column_attrs:
        setattr(to_sql_obj, column.key, getattr(from_sql_obj, column.key))


class RawPlayerStatsManager:

    @classmethod
    def create(cls, **kwargs):
        return util.create(RawPlayerStats, **kwargs)

    @classmethod
    def create_stats(cls, stats):
        util.insert(stats)

    @classmethod
    def delete_stats(cls, stats):
        util.delete(stats)


class PlayerProfileManager:

    @classmethod
    def create(cls, **kwargs):
        return util.create(PlayerProfile, **kwargs)

    @classmethod
    def save_profile(cls, profile):
        target = cls.first_in_name_list(profile.name)
        if target is not None:
            logger.info('profile existed for {}'.format(profile.name))
            id = target.id
            sql_copy(profile, target)
            target.id = id
            util.insert(target)
            return target
        else:
            util.insert(profile)
            return profile

    @classmethod
    def first_by_name(cls, name):
        return session.query(PlayerProfile).filter(PlayerProfile.name == name).distinct().first()

    @classmethod
    def first_in_name_list(cls, name):
        all_profiles = session.query(PlayerProfile).all()
        rets = list(filter(lambda profile: name in json.loads(profile.name_alias), all_profiles))
        if len(rets) > 0:
            return rets[0]
        else:
            return None

    @classmethod
    def delete_stats(cls, profile):
        util.delete(profile)


class RequestRecordManager:

    @classmethod
    def create(cls, **kwargs):
        return util.create(RequestRecord, **kwargs)

    @classmethod
    def save_record(cls, record):
        target = cls.first_by_request(record.request)
        if target is not None:
            sql_copy(record, target)
            util.insert(target)
        else:
            return util.insert(record)

    @classmethod
    def first_by_request(cls, request):
        return session.query(RequestRecord).filter(RequestRecord.request == request).distinct().first()

    @classmethod
    def delete_by_request(cls, request):
        instance = cls.first_by_request(request)
        util.delete(instance)


# todo too redundant
class RequestErrorManager:

    @classmethod
    def create(cls, **kwargs):
        return util.create(RequestError, **kwargs)

    @classmethod
    def save_error(cls, error):
        target = cls.first_by_request(error.request)
        if target is not None:
            sql_copy(error, target)
            util.insert(target)
        else:
            return util.insert(error)

    @classmethod
    def first_by_request(cls, request):
        return session.query(RequestError).filter(RequestError.request == request).distinct().first()

    @classmethod
    def delete_by_request(cls, request):
        instance = cls.first_by_request(request)
        util.delete(instance)
