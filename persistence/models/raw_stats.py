import datetime
import json
import uuid

from sqlalchemy import Column, String, Text, TIMESTAMP, ForeignKey
from sqlalchemy.dialects.postgresql import UUID, JSONB
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from sqlalchemy.orm.state import InstanceState

from utils import common_utils

Base = declarative_base()


class JsonSerializable:
    def to_map(self):
        map = {}
        for key in self.__dict__:
            value = self.__dict__[key]
            if isinstance(value, InstanceState):
                pass
            elif isinstance(value, JsonSerializable):
                map[key] = value.to_json()
            elif isinstance(value, uuid.UUID):
                map[key] = str(value)
            elif isinstance(value, datetime.datetime):
                map[key] = value.timestamp() * 1000
            else:
                map[key] = value
        return map

    def to_json(self):
        return json.dumps(self.to_map())

    def __repr__(self):
        return json.dumps(self.to_json())


class RawPlayerStats(Base, JsonSerializable):
    __tablename__ = 'player_stats_raw'
    __table_args__ = {'schema': 'baseball'}

    name_id = Column(UUID(as_uuid=True), ForeignKey('baseball.player_profile.id'), nullable=False, primary_key=True)
    team = Column(String, primary_key=True)
    stats = Column(JSONB, primary_key=True)
    time_period = Column(JSONB, primary_key=True)
    conditions = Column(JSONB, primary_key=True)
    opponents = Column(JSONB, primary_key=True)
    datasource = Column(String, primary_key=True)
    created_date = Column(TIMESTAMP)
    last_modified_date = Column(TIMESTAMP)

    player_profile = relationship('PlayerProfile', cascade="save-update")


class RequestRecord(Base, JsonSerializable):
    __tablename__ = 'request_management'
    __table_args__ = {'schema': 'baseball'}

    request = Column(Text, primary_key=True)
    handled_date = Column(TIMESTAMP)
    fetched_date = Column(TIMESTAMP)
    parsed_date = Column(TIMESTAMP)
    handler = Column(String)


class RequestError(Base, JsonSerializable):
    __tablename__ = 'request_error'
    __table_args__ = {'schema': 'baseball'}

    request = Column(Text, primary_key=True)
    error_message = Column(Text)
    handled_date = Column(TIMESTAMP)


class PlayerProfile(Base, JsonSerializable):
    __tablename__ = 'player_profile'
    __table_args__ = {'schema': 'baseball'}

    id = Column(UUID(as_uuid=True), nullable=False, primary_key=True)
    name = Column(String)
    bats = Column(String)
    throws = Column(String)
    name_alias = Column(JSONB)
    position = Column(JSONB)
    profile = Column(JSONB)
    contract = Column(JSONB)
    draft = Column(JSONB)

    raw_player_stats = relationship('RawPlayerStats')
