import datetime
import json
import uuid

from persistence.models.raw_stats import RequestRecord, PlayerProfile
from persistence.utils import domain_utils
from utils import archivist

logger = archivist.logger("db tests")


def test_request_record_create():
    record = RequestRecord(request='test3', handled_date=datetime.datetime.now(), handler='tester')
    domain_utils.RequestRecordManager.save_record(record)


def test_request_record_delete():
    domain_utils.RequestRecordManager.delete_by_request('test3')


def test_request_record_by_request():
    ret = domain_utils.RequestRecordManager.first_by_request("test3")
    logger.info(ret)


def test_json():
    record = RequestRecord(request='test3', fetched_date=datetime.datetime.now(), handled_date=datetime.datetime.now(),
                           handler='tester')
    json.dumps(record.to_json())


def test_create_profile():
    profile = PlayerProfile(id=uuid.uuid4(), name='name1', name_alias=json.dumps(['name1']))
    domain_utils.PlayerProfileManager.save_profile(profile)


def test_fetch_profile_by_name():
    profile = domain_utils.PlayerProfileManager.first_by_name('name1')
    print(profile)


def test_fetch_profile_in_name_alias():
    profile = domain_utils.PlayerProfileManager.first_in_name_list('name1')
    print(profile)


def test_save_profile():
    profile = PlayerProfile(id=uuid.uuid4(), name='name1', name_alias=json.dumps(['name1']))
    domain_utils.PlayerProfileManager.save_profile(profile)
    profile2 = PlayerProfile(id=profile.id, name='name1', name_alias=json.dumps(['name3', 'nam2']), bats='R')
    domain_utils.PlayerProfileManager.save_profile(profile2)


test_json()
test_request_record_create()
test_request_record_by_request()
test_request_record_delete()

test_create_profile()
test_fetch_profile_by_name()
test_fetch_profile_in_name_alias()
test_save_profile()
