import time
from multiprocessing import Process


def f1():
    for i in range(50000):
        time.sleep(5)
        print('f1 ' + str(i))


def f2():
    for i in range(10000):
        time.sleep(2)
        print('f2 ' + str(i))


def integ():
    p1 = Process(target=f1)
    p2 = Process(target=f2)
    p2.start()
    p1.start()


integ()
