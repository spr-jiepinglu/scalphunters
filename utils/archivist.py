import logging

# todo add db log and file log
import sys

FORMATTER = logging.Formatter("%(asctime)s — %(name)s — %(levelname)s — %(message)s")


def console_log_handler():
    console_handler = logging.StreamHandler(sys.stdout)
    console_handler.setFormatter(FORMATTER)
    return console_handler


def logger(logger_name):
    archivist = logging.getLogger(logger_name)
    archivist.setLevel(logging.DEBUG)
    archivist.addHandler(console_log_handler())
    return archivist
