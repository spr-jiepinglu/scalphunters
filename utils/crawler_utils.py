from urllib.parse import urlparse

from bs4 import BeautifulSoup


def all_links(html):
    soup = BeautifulSoup(html)
    links = soup.find_all('a')
    return list(map(lambda link: link.get('href'), links))


def first_tag_text(soup, tag):
    return soup.find_all(tag)[0].text.strip()


def has_param(url, param):
    parsed = urlparse(url)
    return param in parsed.query

