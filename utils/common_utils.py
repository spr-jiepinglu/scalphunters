import itertools
import json


def flatmap(list_of_list):
    return list(itertools.chain.from_iterable(list_of_list))


def has_method(obj, func_name):
    return hasattr(obj, func_name)


def json(text):
    try:
        map = json.loads(text)
        return map
    except Exception as e:
        return None
