import random
import time


def cover_up(func):
    def wrapper(*args, **kwargs):
        headers = {
            "User-Agent": 'Safari/537.36'
        }
        kwargs['coverup-headers'] = headers
        time.sleep(random.randrange(0, 3))
        return func(*args, **kwargs)

    return wrapper
