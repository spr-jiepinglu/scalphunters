import json

from kafka import KafkaProducer, KafkaConsumer

from settings import constants, configs

key_serializer = str.encode

bootstrap_servers = [constants.kafka_url + ':' + constants.kafka_port]


def value_serializer(value):
    return json.dumps(value).encode('utf-8')


def value_deserializer(value):
    return json.loads(value.decode('ascii'))


# simple producer
def exporter():
    producer = KafkaProducer(bootstrap_servers=bootstrap_servers, value_serializer=value_serializer,
                             key_serializer=key_serializer)
    return producer


# simple consumer
def letter_box(group_id):
    offset = 'earliest' if configs.CONSUMER_REPLAY else 'latest'
    consumer = KafkaConsumer(bootstrap_servers=bootstrap_servers, value_deserializer=value_deserializer,
                             group_id=group_id, auto_offset_reset=offset)
    return consumer
