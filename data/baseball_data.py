import datetime


class PlayerStatsRaw:
    def __init__(self, name_id, subject, stats, time, splits, opponents, created_date,
                 last_updated=datetime.datetime.now()):
        self.name_id = name_id
        self.subject = subject
        self.stats = stats
        self.time = time
        self.splits = splits
        self.opponents = opponents
        self.last_updated = last_updated
        self.created_date = created_date
