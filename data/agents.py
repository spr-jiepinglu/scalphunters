import names

from utils import archivist

logger = archivist.logger("Agent System")


class Agent:

    def __init__(self):
        self.name = self.__class__.__name__ + ' ' + names.get_full_name()
        logger.info("New Agent Created, name {}".format(self.name))


# assign web pages for actual parsing - find relevant stats page, then send to topic
class Commander(Agent):

    def assign_page(self):
        pass


# parse page to data structures
class Scout(Agent):

    def parse_page(self):
        pass


# handle incoming data then persistent or quarantine or any extra handling
class Analyst(Agent):

    def handle_data(self):
        pass
